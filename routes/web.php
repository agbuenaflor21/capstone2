<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Show Menu
Route::get('/menu', 'ItemController@index');
// Search
Route::post('/search', 'ItemController@search');
// Filter by category in Menu
Route::get('/menu/{id}', 'ItemController@filter');
// Filter by price in Menu
Route::get('/menu/sort/{sort}', 'ItemController@sort');
















// Middleware (admin) ----------------------------
Route::middleware('admin')->group(function(){

// add item
Route::get('/additem', 'ItemController@create');
// store/save item
Route::post('/additem', 'ItemController@store');
// delete item
Route::delete('/deleteitem/{id}', 'ItemController@destroy');
// edit item - to go to edit form
Route::get('/edititem/{id}', 'ItemController@edit');
// update item - save the edited item
Route::patch('/edititem/{id}', 'ItemController@update');
// show all orders
Route::get('/allorders', 'OrderController@allOrders');
// users - show all users - change role - delete user
Route::get('/allusers', 'UserController@index');
Route::get('/changerole/{id}', 'UserController@changeRole');
Route::delete('/deleteuser/{id}', 'UserController@destroy');


});









// Middleware (user1) ----------------------------
Route::middleware('user')->group(function(){

//Cart CRUD
// Add to cart
Route::post('/addtocart/{id}', 'ItemController@addToCart');
// Show cart
Route::get('/showcart', 'ItemController@showCart');
// Delete item from cart
Route::delete('/removeitem/{id}', 'ItemController@removeItem');
// checkout
Route::get('/checkout', 'OrderController@checkout');
// show orders of users
Route::get('/showorders', 'OrderController@showOrders');
// empty cart
Route::delete('/removeall', 'ItemController@removeAll');


});




// Middleware (user2) -----------------------------
Route::middleware('user')->group(function(){

// cancel order by admin
Route::get('/cancelorder/{id}', 'OrderController@cancelOrderByAdmin');

});


