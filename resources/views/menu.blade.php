@extends("layouts.app")
@section("content")

<body style="background-image: url('{{ asset('images/bg3.jpg')}}'); background-position: center; background-size: cover;">
{{-- Title --}}
<h1 class="text-center py-5" style="color: white; text-shadow: 2px 2px 4px #000000; text-decoration-line: overline underline;">Menu</h1>
{{-- Search Bar --}}
<div class="d-flex justify-content-end">
	<div class="col-lg-2">
		<form class="p-3" action="/search" method="POST">
			@csrf
			<div class="input-group">
			<input type="text" name="search" class="form-control" placeholder="Search items by name, category">
			<div class="input-group-append">
				<button class="btn btn-info" type="submit">Search</button>
			</div>

			</div>
			
		</form>
		
	</div>
</div>


{{-- Message --}}
<div class="text-center">
@if(Session::has("message"))
	<h4>{{Session::get('message')}}</h4>
@endif
</div>

{{-- Filter by category --}}
<div class="container">
	<div class="column">
		<div class="col-lg-2">
		<div class="d-flex">
		{{-- <h4>Categories</h4> --}}
		
		<ul class="d-flex d-inline">
			@foreach($categories as $category)
			<li class="list-group-item" style="background-image: url('{{ asset('images/bg4.jpg')}}'); background-position: center; background-size: ;">
				<a href="/menu/{{$category->id}}">{{$category->name}}</a>
			</li>
			@endforeach
			<li class="list-group-item" style="background-image: url('{{ asset('images/bg4.jpg')}}'); background-position: center; background-size: ;">
				<a href="/menu">All</a>
			</li>
			
		</ul>
		
		{{-- <hr>

		<h4>Sort by Price</h4>
		<ul class="list-group">
			<li class="list-group-item">
				<a href="/menu/sort/asc">Cheapest First</a>
			</li>
			<li class="list-group-item">
				<a href="/menu/sort/desc">Most Expensive First</a>
			</li>
		</ul> --}}
		</div>
	</div>
<div class="col-lg-12">
<div class="row w-100">
	@foreach($items as $indiv_item)
	<div class="col-lg-3 my-2">
		<div class="card text-center" style="background-image: url('{{ asset('images/bg4.jpg')}}'); background-position: center; background-size: cover;">
			<img class="card-img-top" src="{{asset($indiv_item->imgPath)}}" alt="Nothing" height="150px">
			<div class="card-body">
				<h5 class="card-title">{{$indiv_item->name}}</h5>
				<p class="card-text">{{$indiv_item->description}}</p>
				<p class="card-text">Price: {{$indiv_item->price}}</p>
				<p class="card-text">Category: {{$indiv_item->category->name}}</p>
			</div>
			@Auth
			@if(Auth::user()->role_id==1)
			<div class="card-footer d-flex justify-content-center align-items-center">
				<form action="/deleteitem/{{$indiv_item->id}}" method="POST">
					@csrf
					@method('DELETE')
					<button class="btn btn-danger" type="submit">DELETE</button>
				</form>
				<a href="/edititem/{{$indiv_item->id}}" class="btn btn-success">Edit</a>
				
			</div>
			@endif
			@endAuth
			@Auth
			@if(Auth::user()->role_id==2)
			<div class="card-footer">
				<input type="number" name="quantity" class="form-control" value="1" id="quantity_{{$indiv_item->id}}">
				<button class="btn btn-primary" onclick="addToCart({{$indiv_item->id}})" type="submit">Add to Cart</button>
			</div>
			@endif
			@endAuth

		</div>
	</div>
	@endforeach
</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	const addToCart = id =>{
		let quantity = document.querySelector("#quantity_"+id).value;
		let data = new FormData;

		data.append("_token", "{{csrf_token()}}");
		data.append("quantity", quantity);

		
		// fetch
		fetch("/addtocart/" + id, {
			method: "POST",
			body: data
		}).then(res=>res.text())
		.then(res=>concole.log(res))

	}


</script>



{{-- 
<body style="background-image: url('{{ asset('images/bg3.jpg')}}'); background-position: center; background-size: cover;">
<div class="container" >
	<div class="row" >
		@foreach($items as $indiv_item)
		<div class="col-lg-4 my-2">
			<div class="card text-center" style="background-image: url('{{ asset('images/bg4.jpg')}}'); background-position: center; background-size: cover;">
			<img class="card-body" src="{{ $indiv_item->imgPath }}" alt="Nothing" height="300px">
			<div class="card-body">
				<h4 class="card-title">{{ $indiv_item->name }}</h4>
				<p class="card-text">{{ $indiv_item->price }}</p>
				<p class="card-text">{{ $indiv_item->description }}</p>
				<p class="card-text">{{ $indiv_item->category->name }}</p>
			</div>
			<div class="footer d-flex justify-content-center">
				<form action="/deleteitem/{{ $indiv_item->id }}" method="POST">
					@csrf
					@method('DELETE')
					<button class="btn btn-danger" type="submit">Delete Item</button>
					
				</form>
					<a href="/edititem/{{ $indiv_item->id }}" class="btn btn-success">Edit Item</a>
				
			</div>
			<div class="card-footer">
				<form action="/addtocart/{{$indiv_item->id }}" method="POST">
					@csrf
					<input type="number" name="quantity" class="form-control" value="1">
					<button class="btn btn-primary" type="submit">Add to Cart</button>
				</form>
			</div>	
			</div>		
		</div>
		@endforeach
	</div>
	
</div>

</body>
 --}}

@endsection