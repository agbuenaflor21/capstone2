<?php

namespace App\Http\Controllers;
use Auth;
use \App\Order;
use \App\Item;
use Session;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function checkout(){
    	// Save the transaction in the order table
    	// Make sure that there is a user logged in
    	// Populate our item_order;

    	if(Auth::user()){

    		$order = new Order;
    		$order->user_id = Auth::user()->id;
    		$order->status_id = 1; //pending
    		$order->payment_id = 1; //COD
    		$order->total = 0;
    		$order->save();

    		$cart = Session::get('cart');

    		$total = 0;
    		foreach($cart as $itemId=>$quantity){
    			// syntax for adding entries to our many to many table
    			$order->items()->attach($itemId, ["quantity"=>$quantity]);

    			$item = Item::find($itemId);
    			$total += $item->price *$quantity;
    		}


    		$order->total = $total;
    		$order->save();

    		Session::forget('cart');
    		Session::flash('message', "Order successfully placed");
    		return redirect('/menu');
    	}else{
    		return redirect('/login');
    	}
    }

    public function showOrders(){
    	$orders = Order::where('user_id', Auth::user()->id)->get();
    	return view('userviews.orders', compact('orders'));
    }

    public function allOrders(){
    	$orders = Order::all();
    	return view('adminviews.allorders', compact('orders'));
    }

    public function cancelOrderByAdmin($id){
    	$order = Order::find($id);

    	if(Auth::user()->role_id ==2){
    		$order->status_id = 3;
    		$order->save();
    		return redirect("/showorders");
    	}else{
    		$order->status_id = 4;
    	$order->save();

    	return redirect("/allorders");
    	}
    }
}
